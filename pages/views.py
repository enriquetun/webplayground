#from django.shortcuts import render, get_object_or_404, get_list_or_404
import csv #lo importe para poder usar exel
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from django.http import HttpResponse #Me sirvio como respuesta para exel
from .models import Page
from .forms import PageForm
from .mixins import StaffRequiredMixin
"""Decoradores jajaja """
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
# Create your views here.



# vista basadas en clases  es mejor
class PageListView(ListView):
    model = Page

''' def pages(request):
    pages = get_list_or_404(Page)
    return render(request, 'pages/pages.html', {'pages':pages}) '''

class PageDetailView(DetailView):
    model = Page
''' def page(request, page_id, page_slug):
    page = get_object_or_404(Page, id=page_id)
    return render(request, 'pages/page.html', {'page':page}) '''

@method_decorator(staff_member_required, name='dispatch')
class PageCreate(CreateView):
    model = Page
    form_class = PageForm
    # fields = ['title', 'content', 'order']
    success_url = reverse_lazy('pages:pages')
    # def get_success_url(self):
       # return reverse('pages:pages')
    
 
class PageUpdateView(StaffRequiredMixin, UpdateView):
    model = Page
    form_class = PageForm
    # fields = ['title', 'content', 'order']
    template_name_suffix = '_update_form' #Sufijo para buscar template para editar
    # success_url = reverse_lazy('pages:pages')

    def get_success_url(self):
        return reverse_lazy('pages:update', args=[self.object.id]) + '?ok'


class PageDelete(StaffRequiredMixin, DeleteView):
    model = Page
    success_url = reverse_lazy('pages:pages')


def ExportPagesCsv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="page.csv"'

    write = csv.writer(response)
    write.writerow(['title', 'content', 'order'])

    pages = Page.objects.all().values_list('title', 'content', 'order')
    for page_row in pages:
        write.writerow(page_row)

    
    return response